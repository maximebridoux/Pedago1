#ifndef CODEINTERFACE_H
#define CODEINTERFACE_H

#include <QCoreApplication>
#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QLabel>

#include <stdio.h>


class CodeInterface : public QWidget
{
    Q_OBJECT
public:
    CodeInterface(QWidget *parent, int w, int h);
    void show_interface();
    QGraphicsScene *scene;
    QGraphicsView *view;
    QLabel *algo;
    void text(int i);

};

#endif // CODEINTERFACE_H
