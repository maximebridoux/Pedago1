#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>
#include <QApplication>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QErrorMessage>

#include "animwindow.h"
#include "comparewindow.h"
#include "sort_name_list.h"

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

private:
    std::vector<QCheckBox*> sort;
    QHBoxLayout *layout1;
    QVBoxLayout *layout2;
    QPushButton *button_anim;
    QPushButton *button_compare;
    QPushButton *button_exit;
    std::vector<int> compute_sort_list();
    void generate_all_QCheck_box();

public slots:
    void launch_anim();
    void launch_compare_anim();
};

#endif // MAINWINDOW_H
