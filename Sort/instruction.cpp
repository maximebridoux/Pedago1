#include "instruction.h"

Instruction::Instruction() : a(-1), b(-1), c(Qt::gray), op(-1)
{}


Instruction::Instruction(int _a, int _b, QColor _c, int _op) :
    a(_a), b(_b), c(_c), op(_op)
{}

int Instruction::get_a()
{
    return a;
}

int Instruction::get_b()
{
    return b;
}

QColor Instruction::get_color()
{
    return c;
}

int Instruction::get_op()
{
    return op;
}
