#include "comparewindow.h"


CompareWindow::CompareWindow(QWidget *parent, std::vector<int> _sort_list) :
    QWidget(parent)
{
    //liste des tris cochés
    sort_list = _sort_list;
    //Tri sur pause
    ongoing = false;

    //layout principal de la fenêtre, contient anim + widgets
    QHBoxLayout *layout_column = new QHBoxLayout(this);

    //layout principal pour ranger les widgets (boutons, ...)
    QVBoxLayout *layout_buttons_1 = new QVBoxLayout();
    //Bouton mélanger
    QPushButton *button_shuffle = new QPushButton("Mélanger", this);
    layout_buttons_1->addWidget(button_shuffle);
    //Bouton trier
    QPushButton *button_sort = new QPushButton("Trier", this);
    layout_buttons_1->addWidget(button_sort);
    //Bouton pas à pas
    QPushButton *button_step = new QPushButton("Pas à pas", this);
    layout_buttons_1->addWidget(button_step);

    //layout contenant le petit bout concernant le nombre d'items
    QVBoxLayout *layout_options = new QVBoxLayout();
    //Texte
    number_of_items_label = new QLabel("Nombre d'items à trier");
    layout_options->addWidget(number_of_items_label);
    //layout contenant slider et écran lcd
    QHBoxLayout *layout_options_1 = new QHBoxLayout();
    //Slider
    number_of_items_slider = new QSlider(Qt::Horizontal);
    number_of_items_slider->setMinimum(2);
    number_of_items_slider->setMaximum(200);
    number_of_items_slider->setValue(35);
    layout_options_1->addWidget(number_of_items_slider);
    //Ecran LCD
    number_of_items = new QLCDNumber();
    number_of_items->display(35);
    number_of_items->setSegmentStyle(QLCDNumber::Flat);
    layout_options_1->addWidget(number_of_items);
    layout_options->addLayout(layout_options_1);

    speed_label = new QLabel("Délai d'affichage");
    layout_options->addWidget(speed_label);
    //layout contenant slider et écran lcd
    QHBoxLayout *layout_options_2 = new QHBoxLayout();
    //Slider
    speed_slider = new QSlider(Qt::Horizontal);
    speed_slider->setMinimum(1);
    speed_slider->setMaximum(100000);
    speed_slider->setValue(1000);
    layout_options_2->addWidget(speed_slider);
    //Ecran LCD
    speed = new QLCDNumber();
    speed->display(1000);
    speed->setSegmentStyle(QLCDNumber::Flat);
    layout_options_2->addWidget(speed);

    layout_options->addLayout(layout_options_2);
    //Bouton stop
    QPushButton *button_stop = new QPushButton("Pause");
    layout_options->addWidget(button_stop);

    layout_buttons_1->addLayout(layout_options);

    //Pousser les widgets vers le haut
    if(sort_list.size() <= 2)
    {
        layout_buttons_1->addSpacing(000);
    }
    else
    {
        layout_buttons_1->addSpacing(300);
    }

    QPushButton *button_close = new QPushButton("Fermer");
    layout_options->addWidget(button_close);


    //Initialiser toutes les interfaces
    anim_interfaces = new std::vector<AnimInterface*>();
    for (int k = 0; k < sort_list.size(); k++)
    {
        anim_interfaces->push_back(new AnimInterface(this, 500, 300));
    }

    //Créer une grille de layout
    QGridLayout *layout_view_grid = new QGridLayout();
    Sort_name_list s;
    QFont font( "Bavaria" );
    font.setPointSize( 24 );
    font.setWeight( QFont::Bold );
    for (int k = 0; k < sort_list.size(); k++)
    {
        //Mettre un layout par case
        QVBoxLayout *layout_view = new QVBoxLayout();
        //Mettre du texte avec le nom du tri dedans
        QLabel *text = new QLabel(s.get_associated_sort(sort_list[k]));
        text->setFont(font);
        layout_view->addWidget(text);
        //Mettre l'interface dedans
        layout_view->addWidget(anim_interfaces->at(k)->view);

        //Mettre ce layout dans la case n°
        if(sort_list.size() <= 2)
        {
            layout_view_grid->addLayout(layout_view,0, k);
        }
        else
        {
            layout_view_grid->addLayout(layout_view,k%2, k/2);
        }

    }

    layout_column->addLayout(layout_view_grid);
    layout_column->addLayout(layout_buttons_1);

    //Connecter les différents boutons et signaux
    QObject::connect(button_shuffle, SIGNAL(clicked()), this, SLOT(order_shuffle()));
    QObject::connect(button_sort, SIGNAL(clicked()), this, SLOT(sort()));
    QObject::connect(button_step, SIGNAL(clicked()), this, SLOT(step_by_step()));
    QObject::connect(number_of_items_slider, SIGNAL(valueChanged(int)), number_of_items, SLOT(display(int)));
    QObject::connect(speed_slider, SIGNAL(valueChanged(int)), speed, SLOT(display(int)));
    QObject::connect(button_stop, SIGNAL(clicked()), this, SLOT(stop()));
    QObject::connect(button_close, SIGNAL(clicked()), this, SLOT(close()));
    for (int k = 0; k < anim_interfaces->size(); k ++)
    {
        QObject::connect(anim_interfaces->at(k), SIGNAL(sorted()),
                         this, SLOT(check()));
    }
}

void CompareWindow::prepare()
{
    has_ended.clear();
    for (int k = 0; k < sort_list.size(); k++)
    {
        has_ended.push_back(false);
    }
}

void CompareWindow::check()
{
    bool all_done = true;
    for (int k = 0; k < has_ended.size(); k++)
    {
        if (!has_ended[k])
        {
            if (anim_interfaces->at(k)->sort.is_done())
            {
                has_ended[k] = true;
            }
            else
            {
                all_done = false;
            }
        }
    }
    if (all_done)
    {
        ongoing = false;
        prepare();
    }
}

void CompareWindow::sort()
{
    ongoing = true;
    while (ongoing)
    {
        step_by_step();
        usleep(speed_slider->value());
    }
    step_by_step();
    step_by_step();
}

void CompareWindow::step_by_step()
{
    for (int k = 0; k < anim_interfaces->size(); k ++)
    {
        anim_interfaces->at(k)->next_step();
    }
    for (int k = 0; k < anim_interfaces->size(); k ++)
    {
        anim_interfaces->at(k)->draw_items();
    }
}

void CompareWindow::order_shuffle()
{
    for (int k = 0; k < anim_interfaces->size(); k ++)
    {
        anim_interfaces->at(k)->init(number_of_items_slider->value());
        anim_interfaces->at(k)->shuffle();
        anim_interfaces->at(k)->prepare_sort(sort_list[k]);
    }
}

void CompareWindow::stop()
{
    ongoing = false;
}
