#include "sort_name_list.h"

Sort_name_list::Sort_name_list()
{
    name_list = new std::vector<const char *>;
    name_list->push_back("Tri par sélection");
    name_list->push_back("Tri par insertion");
    name_list->push_back("Tri à bulle");
    name_list->push_back("Tri shaker");
    name_list->push_back("Tri rapide");
    name_list->push_back("Tri fusion");
}

std::vector<const char*> Sort_name_list::list_avaible_sorts()
{
    return *name_list;
}

const char* Sort_name_list::get_associated_sort(unsigned int i)
{
    if (i < name_list->size())
    {
        return name_list->at(i);
    }
    else
    {
        return "Erreur";
    }
}

Sort Sort_name_list::create_sort(int i, std::vector<int> &items)
{
    switch (i) {
    case 0:
        return Selection(items);
    case 1:
        return Insertion(items);
    case 2:
        return Bubble(items);
    case 3:
        return Shaker(items);
    case 4:
        return Quick(items);
    case 5:
        return Merge(items);
    default:
        return Insertion(items);
    }
}
