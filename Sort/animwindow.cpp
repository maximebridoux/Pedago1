#include "animwindow.h"


AnimWindow::AnimWindow(QWidget *parent, std::vector<int> _sort_list) :
    QWidget(parent)
{
    setFixedSize(1094,660);
    //liste des tris cochés
    sort_list = _sort_list;
    //Tri sur pause
    ongoing = false;
    //interface de l'animation
    anim_interface = new AnimInterface(this, 900, 600);

    QVBoxLayout *layout_all = new QVBoxLayout(this);

    //layout principal de la fenêtre, contient anim + widgets
    QHBoxLayout *layout_column = new QHBoxLayout();

    //layout principal pour ranger les widgets (boutons, ...)
    QVBoxLayout *layout_buttons_1 = new QVBoxLayout();
    //Bouton mélanger
    QPushButton *button_shuffle = new QPushButton("Mélanger", this);
    layout_buttons_1->addWidget(button_shuffle);
    //Bouton trier
    QPushButton *button_sort = new QPushButton("Trier", this);
    layout_buttons_1->addWidget(button_sort);
    //Bouton pas à pas
    QPushButton *button_step = new QPushButton("Pas à pas", this);
    layout_buttons_1->addWidget(button_step);

    //layout contenant le petit bout concernant le nombre d'items
    QVBoxLayout *layout_options = new QVBoxLayout();
    //Texte
    number_of_items_label = new QLabel("Nombre d'items à trier");
    layout_options->addWidget(number_of_items_label);
    //layout contenant slider et écran lcd
    QHBoxLayout *layout_options_1 = new QHBoxLayout();
    //Slider
    number_of_items_slider = new QSlider(Qt::Horizontal);
    number_of_items_slider->setMinimum(2);
    number_of_items_slider->setMaximum(200);
    number_of_items_slider->setValue(35);
    layout_options_1->addWidget(number_of_items_slider);
    //Ecran LCD
    number_of_items = new QLCDNumber();
    number_of_items->display(35);
    number_of_items->setSegmentStyle(QLCDNumber::Flat);
    layout_options_1->addWidget(number_of_items);
    layout_options->addLayout(layout_options_1);
    //Bouton stop

    speed_label = new QLabel("Délai d'affichage");
    layout_options->addWidget(speed_label);
    //layout contenant slider et écran lcd
    QHBoxLayout *layout_options_2 = new QHBoxLayout();
    //Slider
    speed_slider = new QSlider(Qt::Horizontal);
    speed_slider->setMinimum(1);
    speed_slider->setMaximum(100000);
    speed_slider->setValue(1000);
    layout_options_2->addWidget(speed_slider);
    //Ecran LCD
    speed = new QLCDNumber();
    speed->display(1000);
    speed->setSegmentStyle(QLCDNumber::Flat);
    layout_options_2->addWidget(speed);


    layout_options->addLayout(layout_options_2);

    QPushButton *button_stop = new QPushButton("Pause");
    layout_options->addWidget(button_stop);

    button_code = new QPushButton("Pseudo code");
    layout_options->addWidget(button_code);

    layout_buttons_1->addLayout(layout_options);


    //Layout contenant choix du tri
    QVBoxLayout *layout_buttons_2 = new QVBoxLayout();
    QHBoxLayout *layout_buttons_2_2 = new QHBoxLayout();
    //Flèche gauche
    button_previous = new QPushButton("<--", this);
    layout_buttons_2_2->addWidget(button_previous);
    //Flèche droite
    button_next = new QPushButton("-->", this);
    layout_buttons_2_2->addWidget(button_next);
    //Bouton fermer
    QPushButton *button_close = new QPushButton("Fermer");
    layout_buttons_2->addLayout(layout_buttons_2_2);
    layout_buttons_2->addWidget(button_close);
    layout_buttons_1->addSpacing(300);

    layout_buttons_1->addLayout(layout_buttons_2);

    //Pousser les widgets vers le haut

    layout_column->addWidget(anim_interface->view);
    layout_column->addLayout(layout_buttons_1);

    //Connecter les différents boutons et signaux
    QObject::connect(button_shuffle, SIGNAL(clicked()), this, SLOT(order_shuffle()));
    QObject::connect(button_sort, SIGNAL(clicked()), this, SLOT(sort()));
    QObject::connect(button_step, SIGNAL(clicked()), this, SLOT(step_by_step()));
    QObject::connect(anim_interface, SIGNAL(sorted()), this, SLOT(animation_ended()));
    QObject::connect(button_next, SIGNAL(clicked()), this, SLOT(next_sort()));
    QObject::connect(button_previous, SIGNAL(clicked()), this, SLOT(previous_sort()));
    QObject::connect(number_of_items_slider, SIGNAL(valueChanged(int)), number_of_items, SLOT(display(int)));
    QObject::connect(speed_slider, SIGNAL(valueChanged(int)), speed, SLOT(display(int)));
    QObject::connect(button_stop, SIGNAL(clicked()), this, SLOT(stop()));
    QObject::connect(button_close, SIGNAL(clicked()), this, SLOT(close()));
    QObject::connect(button_code, SIGNAL(clicked()), this, SLOT(toggle_code()));

    button_code->setDisabled(true);

    //Texte
    QFont font( "Bavaria" );
    font.setPointSize( 24 );
    font.setWeight( QFont::Bold );
    label_current_sort = new QLabel("Aucun tri chargé");
    label_current_sort->setFont(font);
    layout_all->addWidget(label_current_sort);
    code_interface = new CodeInterface(this, 500,600);
    layout_column->addWidget(code_interface->view);
    layout_all->addLayout(layout_column);
    code_interface->view->close();
}

void AnimWindow::prepare()
{
    //Charge le premier tri par défaut de la liste des tris chargés
    i_current_sort = 0;
    //Met à jour le texte du tri
    update_label_text();
    //Met à jour les boutons flèche cliquables
    update_next_buttons();
}

void AnimWindow::sort()
{
    ongoing = true;
    while (ongoing)
    {
        step_by_step();
        //Pause en microsecondes
        usleep(speed_slider->value());
    }
    step_by_step();
}

void AnimWindow::step_by_step()
{
    anim_interface->next_step();
    //Redessine l'interface
    anim_interface->draw_items();
}

void AnimWindow::animation_ended()
{
    ongoing = false;
}

int AnimWindow::get_selected_sort()
{
    return sort_list[i_current_sort];
}

void AnimWindow::order_shuffle()
{
    ongoing = false;
    anim_interface->important = -1;
    //Initialise les n éléments de la liste à trier
    anim_interface->init(number_of_items_slider->value());
    //Mélange de la liste à trier
    anim_interface->shuffle();
    //Calcul des instructions de tri avec le tri sélectionné
    anim_interface->prepare_sort(get_selected_sort());
}

void AnimWindow::update_label_text()
{
    Sort_name_list s;
    label_current_sort->setText(s.get_associated_sort(get_selected_sort()));
}

void AnimWindow::next_sort()
{
    if (i_current_sort < sort_list.size() - 1)
    {
        i_current_sort++;
        update_window();
    }
}

void AnimWindow::previous_sort()
{
    if (i_current_sort > 0)
    {
        i_current_sort--;
        update_window();
    }
}

void AnimWindow::update_window()
{
    update_next_buttons();
    update_label_text();
    anim_interface->prepare_sort(get_selected_sort());
}

void AnimWindow::update_next_buttons()
{
    if (i_current_sort == 0)
    {
        button_previous->setDisabled(true);
    }
    else
    {
        button_previous->setEnabled(true);
    }
    if (i_current_sort == sort_list.size()-1)
    {
        button_next->setDisabled(true);
    }
    else
    {
        button_next->setEnabled(true);
    }
}

void AnimWindow::stop()
{
    ongoing = false;
}

void AnimWindow::toggle_code()
{
    if (code_interface->view->isVisible())
    {

        code_interface->view->close();
        setFixedSize(1094,660);

    }
    else
    {
        setFixedSize(1100+code_interface->view->width(),660);

        code_interface->show_interface();
    }
}
