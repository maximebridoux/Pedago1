#ifndef SHAKER_H
#define SHAKER_H

#include "sort.h"

class Shaker : public Sort
{
public:
    Shaker(std::vector<int> items);

    void compute_sort(std::vector<int> items);
};

#endif // SHAKER_H
