#include "selection.h"

//Constructeur de base, ne pas toucher
Selection::Selection(std::vector<int> items) : Sort(items)
{
    compute_sort(items);
}

//Fonction prototype de base, modifier le tri à l'intérieur
//items est la liste à trier

//add_instruction(i,j,bool) est l'instruction pour l'animation.
//elle commande un échange dans l'animation entre items[i] et items[j]

//bool dit si l'échange est important et sera affiché automatiquement
//en rouge : d'abord i, puis j
//bool false permet de faire les échanges non animés
//ex: pour le tri par Selection le décalage des indices.

//swap(items,i,j) est une fonction du type parent "Sort"
//qui échange items[i] et items[j] dans le vector "items" fourni en argument
//(procède par effet de bord)

void Selection::compute_sort(std::vector<int> items)
{
    int n = items.size();
    for (unsigned int i = 0; i < n-1; i++)
    {
        unsigned int k=i;
        for(unsigned int j = i; j < n; j++)
        {
            add_instruction_color(j,Qt::red);
            if(items[k]>items[j])
            {
                k=j;
            }
        }
        add_instruction_inter_color(i,n-1, Qt::gray);
        add_instruction_color(k, Qt::red);
        swap(items,i,k);
        add_instruction_swap(k,i);
        add_instruction_color(k,Qt::gray);
        add_instruction_color(i,Qt::green);
    }
    add_instruction_color(n-1,Qt::green);
}
