#include "codeinterface.h"

CodeInterface::CodeInterface(QWidget *parent, int w, int h) :
    QWidget(parent)
{
    scene = new QGraphicsScene(this);
    view = new QGraphicsView(scene);
    view->setFixedSize(w,h);
}

void CodeInterface::show_interface()
{
    view->viewport()->repaint();
    view->update();
    view->show();
}
