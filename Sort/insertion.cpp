#include "insertion.h"

//Constructeur de base, ne pas toucher
Insertion::Insertion(std::vector<int> items) : Sort(items)
{
    compute_sort(items);
}

//Fonction prototype de base, modifier le tri à l'intérieur
//items est la liste à trier

//add_instruction(i,j,bool) est l'instruction pour l'animation.
//elle commande un échange dans l'animation entre items[i] et items[j]

//bool dit si l'échange est important et sera affiché automatiquement
//en rouge : d'abord i, puis j
//bool false permet de faire les échanges non animés
//ex: pour le tri par insertion le décalage des indices.

//swap(items,i,j) est une fonction du type parent "Sort"
//qui échange items[i] et items[j] dans le vector "items" fourni en argument
//(procède par effet de bord)

void Insertion::compute_sort(std::vector<int> items)
{
    for (unsigned int i = 0; i < items.size(); i++)
    {
        unsigned int j = 0;
        bool searching = true;
        add_instruction_all_color(Qt::gray);
        add_instruction_color(i,Qt::red);
        while ((j < i) && searching)
        {
            if (items[i] < items[j])
            {
                add_instruction_swap(i,j);
                swap(items,i,j);

                for (unsigned int l = i-1; l > j; l--)
                {
                    swap(items, l, l+1);
                    add_instruction_color(l+1,Qt::green);
                    add_instruction_color(l,Qt::red);
                    add_instruction_swap(l+1,l);
                }
                searching = false;
            }
            j++;
        }
    }
    add_instruction_all_color(Qt::green);
}
