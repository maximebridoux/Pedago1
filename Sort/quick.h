#ifndef QUICK_H
#define QUICK_H

#include "sort.h"

class Quick : public Sort
{
public:
    Quick(std::vector<int> items);

    //Fonction de tri de base
    void compute_sort(std::vector<int> items);
    void quicksort(std::vector<int> items, int left, int right);
};

#endif // QUICK_H
