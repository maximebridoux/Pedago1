#ifndef SORT_NAME_LIST_H
#define SORT_NAME_LIST_H

#include <vector>

#include "sort.h"
#include "selection.h"
#include "insertion.h"
#include "bubble.h"
#include "shaker.h"
#include "quick.h"
#include "merge.h"

class Sort_name_list
{
public:
    Sort_name_list();
    const char* get_associated_sort(unsigned int i);
    std::vector<const char*> list_avaible_sorts();
    Sort create_sort(int i, std::vector<int> &items);
private:
    std::vector<const char *> *name_list;
};

#endif // SORT_NAME_LIST_H
