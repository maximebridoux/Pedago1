#ifndef ANIMINTERFACE_H
#define ANIMINTERFACE_H

#include <QCoreApplication>
#include <QWidget>
#include <QGraphicsScene>
#include <QGraphicsView>

#include "sort.h"
#include "sort_name_list.h"

#include <thread>
#include <chrono>
#include <iostream>
#include <unistd.h>

class AnimInterface : public QWidget
{
    Q_OBJECT
public:
    AnimInterface(QWidget *parent, int w, int h);
    void draw_items();
    void show_interface();

    void init(int i);
    void prepare_sort(int i_sort);
    void sort_items();
    void next_step();

    Sort sort;
    int current_sort;

    int important;

    QGraphicsScene *scene;
    QGraphicsView *view;

private:
    int number_of_items;
    void init_items(unsigned int n);

    std::vector<int> items;
    std::vector<QColor> items_color;
    void shuffle_items();

    Instruction instruction;
    bool in_memory;

signals:
    void sorted();
    void still_running();

public slots:
    void shuffle();

};

#endif // ANIMINTERFACE_H
