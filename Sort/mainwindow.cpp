#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QWidget(parent)
{   
    QHBoxLayout *layout1 = new QHBoxLayout(this);

    QVBoxLayout *layout_left = new QVBoxLayout();

    generate_all_QCheck_box();
    for (unsigned int k = 0; k < sort.size(); k++)
        layout_left->addWidget(sort[k]);

    QVBoxLayout *layout2 = new QVBoxLayout();

    button_anim = new QPushButton("Animer", this);
    layout2->addWidget(button_anim);
    QObject::connect(button_anim, SIGNAL(clicked()), this, SLOT(launch_anim()));

    button_compare = new QPushButton("Comparer", this);
    layout2->addWidget(button_compare);
    QObject::connect(button_compare, SIGNAL(clicked()), this, SLOT(launch_compare_anim()));

    button_exit = new QPushButton("Quitter", this);
    layout2->addWidget(button_exit);
    QObject::connect(button_exit, SIGNAL(clicked()), qApp, SLOT(quit()));

    this->setLayout(layout1);
    layout1->addLayout(layout_left);
    layout1->addLayout(layout2);
}

std::vector<int> MainWindow::compute_sort_list()
{
    std::vector<int> sort_list;
    for (unsigned int k = 0; k < sort.size(); k++)
    {
        if (sort[k]->isChecked())
        {
            sort_list.push_back(k);
        }
    }
    return sort_list;
}

void MainWindow::launch_anim()
{
    std::vector<int> sort_list = compute_sort_list();
    if (sort_list.size() == 0)
    {
        QErrorMessage *error = new QErrorMessage();
        error->showMessage("Erreur : pas de tri sélectionné !");
    }
    else
    {
        AnimWindow *anim_window = new AnimWindow(nullptr, sort_list);
        anim_window->prepare();
        anim_window->show();
        //delete anim_window;
    }
}

void MainWindow::launch_compare_anim()
{
    std::vector<int> sort_list = compute_sort_list();
    if (sort_list.size() == 0)
    {
        QErrorMessage *error = new QErrorMessage();
        error->showMessage("Erreur : pas de tri sélectionné !");
    }
    else
    {
        CompareWindow *compare_window = new CompareWindow(nullptr, sort_list);
        compare_window->prepare();
        compare_window->show();
    }
}

void MainWindow::generate_all_QCheck_box()
{
    Sort_name_list s;
    std::vector<const char *> sort_list = s.list_avaible_sorts();
    for (unsigned k = 0; k < sort_list.size(); k++)
    {
        sort.push_back(new QCheckBox(sort_list.at(k), this));
    }
}
